# Civic Data — Présentation de la démarche

## Approche générale

- Projet OpenSource sous licence GPL
- Fondé sur Drupal 8
- Architecture découplée pour faciliter la mise à disposition de données ouvertes 
au travers d'une API homogène
- Documentation publique de l'API au format OpenAPI 3, au format ReDoc
